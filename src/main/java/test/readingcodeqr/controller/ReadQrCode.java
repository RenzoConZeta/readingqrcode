package test.readingcodeqr.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.ReaderException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;

@RestController
public class ReadQrCode {

	private static final String RUTA_PDF = System.getProperty("user.home") + "/Zxing-tests/Captura";
	
	private static final String RUTA_PDF_1 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-15.11.20.49-GMTDOC003";
	
	private static final String RUTA_PDF_2 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-15.11.24.13-GMTDOC004";
	
	private static final String RUTA_PDF_3 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-15.15.01.20-GMTDOC047";

	private static final String RUTA_PDF_4 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-15.15.01.39-GMTDOC048";
	
	private static final String RUTA_PDF_5 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-15.16.45.05-GMTDOC059";
	
	private static final String RUTA_PDF_6 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-15.16.45.21-GMTDOC060";
	
	private static final String RUTA_PDF_7 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-18.16.33.25-GMT68. Fact Casa Pinheiro";
	
	// Corregido con corrección en el alto
	private static final String RUTA_PDF_8 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-03-21.09.19.47-GMT75. Fact Aires";
	
	private static final String RUTA_PDF_9 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-21.14.29.42-GMTDOC074";
	
	private static final String RUTA_PDF_10 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.04.29-GMTDOC024";
	
	private static final String RUTA_PDF_11 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.04.46-GMTDOC023";
	
	private static final String RUTA_PDF_12 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.05.04-GMTDOC022";
	
	private static final String RUTA_PDF_13 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.05.22-GMTDOC021";
	
	private static final String RUTA_PDF_14 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.06.00-GMTDOC019";
	
	private static final String RUTA_PDF_15 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-21.16.06.15-GMTDOC018";
	
	private static final String RUTA_PDF_16 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.06.38-GMTDOC017";

	private static final String RUTA_PDF_17 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.07.18-GMTDOC015";
	
	private static final String RUTA_PDF_18 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.07.36-GMTDOC014";
	
	private static final String RUTA_PDF_19 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.07.55-GMTDOC013";
	
	private static final String RUTA_PDF_20 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.56.25-GMTDOC002";
	
	private static final String RUTA_PDF_21 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.56.56-GMTDOC004";
	
	private static final String RUTA_PDF_22 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-21.16.57.11-GMTDOC005";
	
	private static final String RUTA_PDF_23 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-03-22.08.56.51-GMTDOC013";
	
	private static final String RUTA_PDF_24 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-22.09.00.18-GMTDOC002";
	
	private static final String RUTA_PDF_25 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-23.14.00.38-GMTDOC007";
	
	private static final String RUTA_PDF_26 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-03-23.14.00.57-GMTDOC008";
	
	private static final String RUTA_PDF_27 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-23.14.01.50-GMTDOC011";
	
	private static final String RUTA_PDF_28 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-23.14.02.06-GMTDOC012";
	
	private static final String RUTA_PDF_29 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-23.14.02.22-GMTDOC013";
	
	private static final String RUTA_PDF_30 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-23.14.03.43-GMTDOC018";
	
	private static final String RUTA_PDF_31 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-23.14.03.59-GMTDOC019";
	
	private static final String RUTA_PDF_32 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-03-23.14.04.15-GMTDOC020";
	
	private static final String RUTA_PDF_33 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-03-23.15.24.54-GMTDOC012";
	
	private static final String RUTA_PDF_34 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-25.16.12.27-GMTFact MCA";
	
	private static final String RUTA_PDF_35 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-28.08.06.27-GMTDOC046";
	
	private static final String RUTA_PDF_36 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-28.08.07.42-GMTDOC042";
	
	private static final String RUTA_PDF_37 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-29.14.10.35-GMTDOC021";
	
	private static final String RUTA_PDF_38 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-29.14.10.51-GMTDOC022";
	
	private static final String RUTA_PDF_39 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-29.14.11.07-GMTDOC023";
	
	private static final String RUTA_PDF_40 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-30.10.26.29-GMTDOC009";
	
	private static final String RUTA_PDF_41 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-03-30.10.27.42-GMTDOC012";
	
	private static final String RUTA_PDF_42 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-01.09.10.54-GMTDOC002";
	
	private static final String RUTA_PDF_43 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-04-04.10.05.14-GMTDOC014";
	
	private static final String RUTA_PDF_44 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-04.14.38.42-GMTDOC020";
	
	private static final String RUTA_PDF_45 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-04.15.13.16-GMTDOC032";
	
	private static final String RUTA_PDF_46 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-04.15.15.43-GMTDOC024";
	
	private static final String RUTA_PDF_47 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-04.15.16.02-GMTDOC023";
	
	private static final String RUTA_PDF_48 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-04-05.15.53.35-GMTDOC012";
	
	private static final String RUTA_PDF_49 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-05.15.54.04-GMTDOC010";
	
	private static final String RUTA_PDF_50 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-07.14.48.00-GMTDOC012";
	
	private static final String RUTA_PDF_51 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-04-07.16.17.33-GMTDOC025";
	
	private static final String RUTA_PDF_52 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-07.16.24.52-GMTDOC026";
	
	private static final String RUTA_PDF_53 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-07.16.33.14-GMTDOC028";
	
	private static final String RUTA_PDF_54 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-07.16.36.21-GMTDOC029";
	
	private static final String RUTA_PDF_55 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-04-07.16.42.00-GMTDOC0122";
	
	private static final String RUTA_PDF_56 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-11.14.55.18-GMTDOC006";
	
	private static final String RUTA_PDF_57 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-11.14.55.37-GMTDOC007";
	
	private static final String RUTA_PDF_58 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-12.09.52.53-GMTDOC018";
	
	private static final String RUTA_PDF_59 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-12.13.36.30-GMTDOC048";
	
	private static final String RUTA_PDF_60 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-12.13.38.23-GMTDOC043";
	
	private static final String RUTA_PDF_61 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-12.13.39.30-GMTDOC040";
	
	private static final String RUTA_PDF_62 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-12.13.40.35-GMTDOC037";
	
	private static final String RUTA_PDF_63 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-12.13.40.52-GMTDOC036";
	
	private static final String RUTA_PDF_64 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-18.15.51.38-GMT116. Fact JMCS";
	
	private static final String RUTA_PDF_65 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-19.13.58.03-GMTDOC006";
	
	private static final String RUTA_PDF_66 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-19.13.58.58-GMTDOC009";
	
	private static final String RUTA_PDF_67 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-19.13.59.17-GMTDOC010";
	
	private static final String RUTA_PDF_68 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-19.13.59.40-GMTDOC011";
	
	private static final String RUTA_PDF_69 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.09.19.55-GMTDOC015";
	
	private static final String RUTA_PDF_70 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.09.20.13-GMTDOC014";
	
	private static final String RUTA_PDF_71 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.13.22.21-GMTDOC035";
	
	private static final String RUTA_PDF_72 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.14.59.50-GMTDOC002";
	
	private static final String RUTA_PDF_73 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.15.00.56-GMTDOC006";
	
	private static final String RUTA_PDF_74 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.15.01.24-GMTDOC008";
	
	private static final String RUTA_PDF_75 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-20.15.01.40-GMTDOC009";
	
	private static final String RUTA_PDF_76 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-21.11.54.26-GMT130. Fact Montirrol";
	
	private static final String RUTA_PDF_77 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-22.09.41.39-GMTDOC009";
	
	private static final String RUTA_PDF_78 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-04-26.15.27.35-GMTDOC002";
	
	private static final String RUTA_PDF_79 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-26.15.32.21-GMTDOC019";
	
	private static final String RUTA_PDF_80 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-26.15.34.03-GMTDOC025";
	
	private static final String RUTA_PDF_81 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-27.08.52.05-GMTDOC013";
	
	private static final String RUTA_PDF_82 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-27.08.52.22-GMTDOC014";
	
	private static final String RUTA_PDF_83 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-27.14.14.54-GMT57. Fact Global Step";
	
	private static final String RUTA_PDF_84 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-28.11.47.38-GMTDOC029";
	
	private static final String RUTA_PDF_85 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-04-28.11.48.49-GMTDOC024";
	
	private static final String RUTA_PDF_86 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-28.11.49.04-GMTDOC023";
	
	private static final String RUTA_PDF_87 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-28.11.49.20-GMTDOC022";
	
	private static final String RUTA_PDF_88 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-04-28.11.49.36-GMTDOC021";
	
	private static final String RUTA_PDF_89 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-28.16.07.26-GMTDOC017";
	
	private static final String RUTA_PDF_90 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-29.09.18.14-GMTDOC023";
	
	private static final String RUTA_PDF_91 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-29.09.18.29-GMTDOC022";
	
	private static final String RUTA_PDF_92 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-04-29.09.18.45-GMTDOC021";
	
	private static final String RUTA_PDF_93 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-05-02.08.13.03-GMTDOC009";
	
	private static final String RUTA_PDF_94 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio fallido/2022-05-02.08.14.34-GMTDOC019";
	
	private static final String RUTA_PDF_95 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-05-02.08.16.25-GMTDOC034";
	
	private static final String RUTA_PDF_96 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/2022-05-02.08.20.06-GMTDOC040";
	
	private static final String RUTA_PDF_97 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-05-02.11.32.33-GMTDOC032";
	
	private static final String RUTA_PDF_98 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Intermedio/2022-05-02.11.32.47-GMTDOC031";
	
	private static final String RUTA_PDF_99 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Bien/2022-05-02.11.34.29-GMTDOC026";
	
	private static final String RUTA_PDF_100 = System.getProperty("user.home") + "/Zxing-tests/Facturas con codigoQR/Error/Corregido/2022-05-02.11.34.52-GMTDOC025";
	
	@GetMapping("pdf-to-img")
	public void pdfToImg() throws IOException {
		final String pathLocal = RUTA_PDF_5;
		PDDocument document = PDDocument.load(new File(pathLocal + ".pdf"));
		PDFRenderer pdfRenderer = new PDFRenderer(document);
		for (int page = 0; page < document.getNumberOfPages(); ++page)
		{ 
		    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

		    ImageIOUtil.writeImage(bim, pathLocal + "-" + (page+1) + ".png", 300);
		    String result = getImageQR(pathLocal + "-" + (page+1) + ".png");
		    System.out.println("result :> " + result);
		}
		document.close();
		System.out.println("listo " + pathLocal);
	}
		
	@GetMapping("get-values-qr")
	public String getImageQR(String path) throws IOException {
		BufferedImage imageb;
		FileInputStream fli = new FileInputStream(path);
		BufferedInputStream bis = new BufferedInputStream(fli);
        imageb = ImageIO.read(bis);
        QRCodeReader reader = new QRCodeReader();
        String result = null;
        try 
        {
        	BufferedImage cropedImageComplete = imageb.getSubimage(imageb.getMinY(), imageb.getMinX(), imageb.getWidth(), imageb.getHeight());
            LuminanceSource sourceComplete = new BufferedImageLuminanceSource(cropedImageComplete);
            BinaryBitmap bitmapComplete = new BinaryBitmap(new HybridBinarizer(sourceComplete));
        	 result = reader.decode(bitmapComplete).getText();
        	 System.out.println("result_1 :>> " + result);
        }
        catch (ReaderException e) 
        {
        	try {
        		BufferedImage cropedImageTop= imageb.getSubimage(0, 0, imageb.getWidth(), imageb.getHeight()/2);
                LuminanceSource sourceTop = new BufferedImageLuminanceSource(cropedImageTop);
                BinaryBitmap bitmapTop = new BinaryBitmap(new HybridBinarizer(sourceTop));
                result = reader.decode(bitmapTop).getText();
                System.out.println("result_2 :>> " + result);
			} catch (ReaderException e2) {
				try {
					BufferedImage cropedImageBottom = imageb.getSubimage(0, imageb.getHeight()/2, imageb.getWidth(), imageb.getHeight()/2);
			        LuminanceSource sourceBottom = new BufferedImageLuminanceSource(cropedImageBottom);
			        BinaryBitmap bitmapBottom = new BinaryBitmap(new HybridBinarizer(sourceBottom));
			        result = reader.decode(bitmapBottom).getText();
			        System.out.println("result_3 :>> " + result);
				} catch (Exception e3) {
					return "Codigo QR no encontrado";
				}
			}
        }
        finally {
        	fli.close();
        }
		return result;

        	
	}
	
	@GetMapping("get-values-img")
	public String getImageQR() throws IOException {
		BufferedImage imageb;
		FileInputStream fli = new FileInputStream(RUTA_PDF+ ".PNG");
		BufferedInputStream bis = new BufferedInputStream(fli);
        imageb = ImageIO.read(bis);
        QRCodeReader reader = new QRCodeReader();
        String result = null;
        System.out.println("result_1 :>> ");
        try 
        {
        	BufferedImage cropedImageComplete = imageb.getSubimage(imageb.getMinY(), imageb.getMinX(), imageb.getWidth(), imageb.getHeight());
            LuminanceSource sourceComplete = new BufferedImageLuminanceSource(cropedImageComplete);
            BinaryBitmap bitmapComplete = new BinaryBitmap(new HybridBinarizer(sourceComplete));
        	 result = reader.decode(bitmapComplete).getText();
        	 System.out.println("result_1 :>> " + result);
        }
        catch (ReaderException e) 
        {
        	try {
        		BufferedImage cropedImageTop= imageb.getSubimage(0, 0, imageb.getWidth(), imageb.getHeight()/2);
                LuminanceSource sourceTop = new BufferedImageLuminanceSource(cropedImageTop);
                BinaryBitmap bitmapTop = new BinaryBitmap(new HybridBinarizer(sourceTop));
                result = reader.decode(bitmapTop).getText();
                System.out.println("result_2 :>> " + result);
			} catch (ReaderException e2) {
				try {
					BufferedImage cropedImageBottom = imageb.getSubimage(0, imageb.getHeight()/2, imageb.getWidth(), imageb.getHeight()/2);
			        LuminanceSource sourceBottom = new BufferedImageLuminanceSource(cropedImageBottom);
			        BinaryBitmap bitmapBottom = new BinaryBitmap(new HybridBinarizer(sourceBottom));
			        result = reader.decode(bitmapBottom).getText();
			        System.out.println("result_3 :>> " + result);
				} catch (Exception e3) {
					return "Codigo QR no encontrado";
				}
			}
        }
        finally {
        	fli.close();
        }
        System.out.println("result_1 :>> " + result);
		return result;

        	
	}
	
}
